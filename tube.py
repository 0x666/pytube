#!/bin/python2
from pytube import YouTube
from optparse import OptionParser
import urllib2
class color:
	P    =  '\033[95m' # purple
	B    =  '\033[94m' # Blue
	BOLD =  '\033[1m'  # Bold
	G    =  '\033[92m' # Green
	Y    =  '\033[93m' # Yellow
	R    =  '\033[91m' # Red
	W    =  '\033[97m' # White
	BL   =  '\033[90m' # Black
	M    =  '\033[95m' # Magenta
	C    =  '\033[96m' # Cyan
	ENDC =  '\033[0m'  # end colors
class Youtube:
	def sizeof(self,bytes):
		alternative = [
        	(1024 ** 5, ' PB'),
        	(1024 ** 4, ' TB'),
        	(1024 ** 3, ' GB'),
        	(1024 ** 2, ' MB'),
        	(1024 ** 1, ' KB'),
        	(1024 ** 0, (' byte', ' bytes')),
    	]
		for factor, suffix in alternative:
			if bytes >= factor:
				break
		amount = int(bytes / factor)
		if isinstance(suffix, tuple):
			singular, multiple = suffix
			if amount == 1:
				suffix = singular
			else:
				suffix = multiple
		return "%s%s" % (str(amount), suffix)
	def print_status(self,progress, file_size, start):
		percent_done = int(progress) * 100. / file_size
		done = int(50 * progress / int(file_size))
		dt = (clock() - start)
		if dt > 0:
			stdout.write("\r  [%s%s][%3.2f%%] %s at %s/s\r " %
				('=' * done, ' ' * (50 - done), percent_done,
					sizeof(file_size), sizeof(progress // dt)))
		stdout.flush()

	def video(self, id, path):
		links = "https://www.youtube.com/watch?v="+id
		get_true = True
		while get_true:
			yt = YouTube(links)
			get_true = False
		#mp4files = yt.filter('mp4')
		print color.W+color.BOLD+"Video Name : "+color.Y+color.BOLD+yt.filename+color.ENDC
		video = yt.get('mp4')
		video.download(path)
	def __init__(self):
		try:
			parser = OptionParser()
			parser.add_option("--id","-i",
			help="ID of Youtube video")
			parser.add_option("--path","-p",
				help="path for save youtube video")
			(options,args) = parser.parse_args()
			id = options.id
			path = options.path
			if id and path:
				self.video(id, path)
		except urllib2.HTTPError:
			print color.R+color.BOLD+"Error Video Not Found !"+color.ENDC
		except OSError:
			print color.R+color.BOLD+"Conflicting filename !"+color.ENDC
		except KeyboardInterrupt:
			print color.R+color.BOLD+"Exiting Now !"+color.ENDC
Youtube()